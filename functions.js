//funzione per ottenere il valore di una variabile che sta nella URL. il parametro da passare Ã¨ il nome della var stessa
function getParameterByName(name){
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var href = window.location.href; 
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp( regexS );
	var results = regex.exec( href );
	if( results == null ){
		return "";
	}else{
		return decodeURIComponent(results[1].replace(/\+/g, " "));
	}
}