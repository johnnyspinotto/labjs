/**
 *	PORKGRILL
 * 	versione 1.0
 * 	sviluppato da Luca Bassani luca@lookabass.com e Enrico Cauteruccio incauto@gmail.com
 * 	
 * 	parametri:
 * 	- numCols: 	numero di colonne della griglia (per esempio 3)
 * 	- snapX:	width di una colonna (per esempio 200)
 * 	- snapY:	altezza del modulo  (per esempio 200)
 * 	- bring2up:	valori ammessi:
 * 					- 0: i box vengono lasciati nella stessa posizione in cui vengono trascinati
 * 					- 1: i box vengono compattati verso l'alto quando hanno uno spazio libero sufficiente
 *
 *	SEQUENZA DI FUNZIONI IN BASE AGLI EVENTI PRINCIPALI 
 *
 *	- CARICAMENTO PAGINA: 
 * 		1. loadData()
 * 		2. boxHtmlData()
 * 		3. customDraggable()
 * 		4. customeResizable()
 * 		5. display()
 * 		6. calcMaxHeight()
 * 		7. move2up()
 * 		8. display()
 * 
 * 	- DRAG:
 * 		1. updateData()
 * 		2. checkScopa()
 * 		3. display()
 * 		4. calcMaxHeight()
 * 		5. move2up()
 * 		6. display()
 * 
 * 	- RESIZE
 * 		1. updateData()
 * 		2. checkScopa()
 * 		3. display()
 * 		4. calcMaxHeight()
 * 		5. move2up()
 * 		6. display()
 */

function pg() {
    this.heightContainer 	= 0; 			// lasciare questo parametro a zero, verrà ricalcolato automaticamente in base ai div contenuti
    this.numCols 			= 3;			// numero di colonne del layout
    this.snapX 				= 200;			// width del box di default
    this.snapY 				= 200;    		// height del box di default
    this.data 				= new Array();	
    this.arrPos 			= new Array();
    this.containerPos 		= $('.container').offset();
    this.bring2up 			= 1;			// 0 / 1: se 1 i blocchi vengono compattati verso l'alto, se 0 rimangono dove posizionati manualmente
    var self 				= this;
       
    this.boxHtmlData = function(items){
        var htmlOut = '';
        $.each(items, function(i,item){
            htmlOut += "<div id='" + item.id + "' class='portlet resizable draggable'>\n\r";
            htmlOut += "    <div class='box'>\n\r";
            htmlOut += "        <div class='portlet-header'>\n\r";
            htmlOut +=              item.title + '\n\r';
            htmlOut += "        </div>\n\r";
            htmlOut += "        <div class='portlet-content'>\n\r";
            htmlOut +=              item.content + '\n\r';
            htmlOut += "        </div>\n\r";
            htmlOut += "    </div>\n\r";
            htmlOut += "</div>\n\r";
        });            
        return htmlOut; 
    }
    
    this.updateChkArea = function(){
    	$.each(self.data, function(i,item){
    		var chkArea = new Array();
    		var chkY = null;
    		for (var i=0; i < item['area'].length; i++){
    			if(chkY == null){
    				chkY = item['area'][i].y;
    			}
    			if(item['area'][i].y == chkY){
    				chkArea.push(item['area'][i].x);
    			}
			}
			item.chkArea = chkArea;
    	});
    }
    
    this.loadData = function(){
        $.ajax({ 
            type: 'POST', 
            url: 'data.json', 
            data: { get_param: 'value' }, 
            dataType: 'json',
            success: function (response) {                     
                self.data = response;
                self.updateChkArea(); 
                var htmlData = self.boxHtmlData(self.data);
                $('#container').html(htmlData);                    
                $('.container').css('width', self.numCols * self.snapX + 'px');                    
                self.customDraggable();
                self.customResizable();
                
                
                self.display();
                self.calcMaxHeight();
                self.move2upV1();
                self.display();
            },
            error: function(error){
                console.log("Chiamata fallita!!!", error);
            }
        });
    }
    
    this.areaOccupata = function (){
    	var boxOccupati = new Array();
    	$.each(self.data,function(index,item){
    		for (i = 0; i < item.area.length; i++){
    			boxOccupati.push(item.area[i]);
    		}    		
    	});
    	return boxOccupati;
    }
    
    this.customDraggable = function (){
        $( ".draggable" ).draggable({ 
            snap: true,
            grid: [ self.snapX, self.snapY ],
            start: function(){                 
                $(this).addClass('zindex');
                var thisBlock = $(this)[0];
                arrPos = $(thisBlock).position();
                var deltaX = 0;
                var deltaY = 0;
                $(document).bind('mousemove', function(e){
                    if(deltaX == 0){
                        deltaX = e.pageX - arrPos.left;
                        deltaY = e.pageY - arrPos.top;
                    }
                    $('#follower').css({
                          'left':       e.pageX - deltaX + $('.container').offset().left,
                          'top':        e.pageY - deltaY + $('.container').offset().top,
                          'width':      thisBlock.clientWidth + 'px',
                          'height':     thisBlock.clientHeight + 'px',
                          'border':     '1px dotted black',
                          'display':    'block',
                          'z-index':    999999999
                    });
                });
            },
            drag: function (){ },
            stop: function (){
                $(document).unbind('mousemove');
                    $('#follower').css({
                        left:       0,
                        top:        0,
                        display:    'none',
                        'z-index':  0
                    });
                $(this).removeClass('zindex');
                  
                var item = self.updateData( $(this) );
                self.chkScopa( item );
                self.display();  
                self.calcMaxHeight();
                self.move2upV1();
                self.display();
            } 
        });    
    }
    
    this.customResizable = function (){
        $('.resizable').resizable({
            minHeight: self.snapX,
            minWidth: self.snapY,
            containment: "#container",
            grid: [self.snapX,self.snapY],
            resize: function (event,$this ){ },
            start: function( event, ui ) {
                arrPos = ui.originalPosition;
                 
                $(document).bind('mousemove', function(e){
                    $('#follower').css({
                          'left':       arrPos.left + $('.container').offset().left,
                          'top':        arrPos.top + $('.container').offset().top ,
                          'width':      (e.pageX - ui.position.left - $('.container').offset().left)+'px',
                          'height':     (e.pageY - ui.position.top - $('.container').offset().top)+'px',
                          'border':     '1px dotted black',
                          'display':    'block',
                          'z-index':    999999999
                    });
                });
            },
            stop: function (event,ui){
                $(document).unbind('mousemove');
                $('#follower').css({
                    left:   0,
                    top:    0,
                    display:    'none',
                    'z-index':  0
                }); 
                var item = self.updateData( ui.element );
                self.chkScopa( item );
                self.display();
                self.calcMaxHeight();
                self.move2upV1();
                self.display();
            }
        }); 
    }
    
    this.calcMaxHeight = function (){
        heightContainer = 0;
        $.each(self.data,function(index,item){
            var heightMax = (item.h * self.snapY) + (item.y * self.snapY);
            if (heightContainer < heightMax){
                heightContainer = heightMax;
            }
        });
        $( ".container" ).css('height',heightContainer + self.snapY + 'px');
    }
    
    this.updateData = function (that){
        var toReturn = '';
        $.each(self.data,function(index,item){
            if (item.id == that[0].id){
                item.x = parseInt(that[0].offsetLeft / self.snapX);
                if(item.x + (item.w - 1) > self.numCols - 1){
                    item.x = self.numCols - item.w; 
                }
                if (item.x < 0 ){
                    item.x = 0;
                }
                item.y = parseInt(that[0].offsetTop / self.snapY);
                if (item.y < 0 ){
                    item.y = 0;
                }
                item.w = parseInt(that[0].offsetWidth / self.snapX);
                item.h = parseInt(that[0].offsetHeight / self.snapY);                 
                item['area'] = self.updateArea(item);
                toReturn = item;              
            }
        });
        return toReturn;
    } 
    
    this.updateArea = function (item){
        var area = new Array();
        for (i = item.x; i < (item.w + item.x); i++){
            for (j = item.y; j < (item.h + item.y); j++){
                area.push({x:i,y:j});
            }
        }
        return area;
    }
    
    this.chkScopa = function (el){    	          
        $.each( self.data, function(index,item){
            var itemCp = item;          
            if (item.id != el.id){
                for (i = el['area'].length - 1; i >= 0; i--){
                    for (j = itemCp['area'].length - 1; j >= 0; j--){
                        if (itemCp['area'][j].x == el['area'][i].x && itemCp['area'][j].y == el['area'][i].y){
                            item.y = item.y + el.h - (item.y - el.y);
                            item['area'] = self.updateArea(item);
                            self.chkScopa(item);
                            return;
                        }
                    }
                }
           }
        });
    }  
    
    this.readPortletOrderOnDisplay = function (){
    	var orderOnDisplay = new Array();
    	for (j = 0; j < (self.heightContainer / self.snapY); j++){
    		for(i = 0; i < self.numCols; i++ ){    		
    			$.each(self.data, function (index,item){
    				if (i == item.x && j == item.y){
    					orderOnDisplay.push(item);
    				}
    			});
    		}
    	}
    	return orderOnDisplay;    	
    }
    
    this.move2upV1 = function (){

    	if (self.bring2up == 1){   
    		portlets = self.readPortletOrderOnDisplay();  	
	        var finitoTutto = 1;  	
	    	$.each( portlets, function(index,item){
	    		if (item.y > 0){
	    			var postoLibero = 1;	    			
	    			for (i = 0; i < item.chkArea.length; i++){    				
	    				$.each(self.data, function (ind,itm) {
	    					if (item.id != itm.id){
		    					for (a = 0; a < itm.area.length; a++){
		    						if (item.y - 1 == itm.area[a].y && item.chkArea[i] == itm.area[a].x){
		    							postoLibero = 0;
		    						}
		    					}
	    					}
	    				});
	    			}
	    			
	    			if (postoLibero == 1){
	    				item.y = item.y - 1;
	    				if (item.y < 0){	item.y = 0; 	}
	    				item.area = self.updateArea(item);
	    				self.updateChkArea();
	    				finitoTutto = 0;
	    				$.each( portlets, function(index,item){		self.chkScopa( item );	});	    				
	    			}
	    		}	    		
	    	});	
	    
	    	if (finitoTutto == 0){
	    		self.move2upV1();
	    	}  	
	    }
    }
        
    this.display = function (){
        $.each(self.data,function(index,item){
            $('#'+item.id).css('float','left');
            $('#'+item.id).css('position','absolute');
            $('#'+item.id).css('left', item.x * self.snapX);
            $('#'+item.id).css('top', item.y * self.snapY);
            $('#'+item.id).css('width', item.w * self.snapX);
            $('#'+item.id).css('height', item.h * self.snapY);            
            heightMax = (item.h * self.snapY) + (item.y * self.snapY);
            if (self.heightContainer < heightMax){
                self.heightContainer = heightMax;
            }            
        });
    }   
} // fine pg       